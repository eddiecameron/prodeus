import pypng.code.png
import random
import shutil
import os
import subprocess
from mutagen.oggvorbis import OggVorbis

creatureImgs = [
    "crabcritter.png",
    "flycritter.png",
    "frogs.png",
    "guidefly.png",
    "mightfly.png",
    "owl.png",
    "quail.png",
    "squirrel.png",
    "tallspirit.png",
    "tallspirit2.png",
    "watson.png",
    "wormcritter.png",
    "wormcritter2.png",
    "wormcritter3.png" ]

worldImgs = [
    "cave.png",
    "cirrus.png",
    "cloud.png",
    "cloudCircle.png",
    "conifers.png",
    "foliage.png",
    "foliageGiant.png",
    "memorials.png"
    ]
    
looping_audio = [
        "eyes_opening.ogg",
        "rain sfx.ogg",
        "slope_ascent1.ogg",
        "slope_ascent2.ogg",
        "slope_ascent3.ogg",
        "spring_slope_ascent.ogg",
        "autumn_slope_ascent.ogg",
        "winter_slope_ascent.ogg",
        "slope_descent1.ogg",
        "slope_descent2.ogg",
        "slope_descent3.ogg",
        "spring_slope_descent.ogg",
        "autumn_slope_descent.ogg",
        "winter_slope_descent.ogg",
        "rain drops.ogg",
        "autumn_rain.ogg",
        "squirrel_dance_bg.ogg",
        "squirrel_sqeak_bg.ogg",
        "squirrel_intree.ogg"
    ]
    
exclude_audio = [
	"graveyard_thick.ogg",
	"graveyard_thin.ogg",
	"monastery hymn (ext).ogg",
	"monastery chant.ogg",
	"autumn_highlands.ogg",
	"autumn bass.ogg",
	"low end peace.ogg",
	"grass.ogg",
	"summer bass.ogg",
	"water tune.ogg"
	]
	
    
resourcePath = ""
if os.name == 'nt':
	resourcePath = "../Proteus/Resources/"
else:
	resourcePath = "../Proteus.app/Contents/MacOS/Resources/"
    
def loadPng( path ):
    r = pypng.code.png.Reader( path )
    return r.read()
    
def copyBitmaps():
    if not os.path.isdir( "../origBitmaps" ):
        shutil.copytree( resourcePath + "Bitmaps", "../origBitmaps" )
    
def copyAudio():
    if not os.path.isdir( "../origAudio" ):
        shutil.copytree( resourcePath + "audio", "../origAudio" )
    
def copyModels():
    if not os.path.isdir( "../origModels" ):
        shutil.copytree( resourcePath + "Models", "../origModels" )
    
def shuffleFiles( fileNames, fromPrefix, toPrefix ):
    shuffledNames = list(fileNames)
    random.shuffle( shuffledNames )
    for i in range( len( fileNames ) ):
        if shuffledNames[i] != fileNames[i]:
            shutil.copy( fromPrefix + fileNames[i], toPrefix + shuffledNames[i] )
    
def shufflePalette():
    loaded = loadPng( "../origBitmaps/palette/default.png" )

    palette = loaded[3]['palette']
    for i in range( len(palette) ):
        colour = palette[i]
        palette[i] = (colour[1], colour[2], colour[0])

    pixels = []
    for line in loaded[2]:
        alteredLine = []
        for pixel in line:
            alteredLine.append( random.randrange( len( palette ) - 1 ) )
        pixels.append( alteredLine )

    w = pypng.code.png.Writer( width=loaded[0], height=loaded[1], palette=palette )
    w.write( open( resourcePath + 'Bitmaps/palette/default.png', 'wb+'), pixels )

def shuffleCreatures():
    shuffleFiles( creatureImgs, "../origBitmaps/", resourcePath + "Bitmaps/" )
        
def shuffleWorld():
    shuffleFiles( worldImgs, "../origBitmaps/", resourcePath + "Bitmaps/" )

def shuffleAudio():
    shortAudioFiles = []
    longAudioFiles = []
    for root, dirs, files in os.walk( '../origAudio' ): 
        print( "NEW ROOT" + root )
        for file in files:
            if file.endswith( ".ogg" ) and file not in exclude_audio:
                name = os.path.join(root, file)
                if 'menu' not in name:
                    oggInfo = OggVorbis( name )
                    if oggInfo.info.length < 2.0:
                        shortAudioFiles.append( name[12:] )
                    else:
                        longAudioFiles.append( name[12:] )
    
    shuffleFiles( shortAudioFiles, "../origAudio", resourcePath + "audio" )
    shuffleFiles( longAudioFiles, "../origAudio", resourcePath + "audio" )

def shuffleModels():

    modelFiles = []

    for filename in os.listdir("../origModels"):

        if filename.endswith(".obj"):

            modelFiles.append( filename[:-4] )

            

    remainingModels = list( modelFiles )

    random.shuffle( remainingModels )

    for i in range( len( modelFiles ) ):

        if modelFiles[i] != remainingModels[i]:

            shutil.copy( "../origModels/" + modelFiles[i] + ".obj", resourcePath + "Models/" + remainingModels[i] + ".obj" )

            shutil.copy( "../origModels/" + modelFiles[i] + ".mtl", resourcePath + "Models/" + remainingModels[i] + ".mtl" )
            

    shutil.copy( "../origBitmaps/RaggedBuildingTextures.png", resourcePath + "Bitmaps/BuildingTextures.png" )

    shutil.copy( "../origBitmaps/conifers.png", resourcePath + "Bitmaps/trees.png" )
    shutil.copy( "../origBitmaps/foliage.png", resourcePath + "Bitmaps/blobbytree.png" )
    shutil.copy( "../origModels/Textures004.png", resourcePath + "Bitmaps/Textures004.png" )
  

try:
    copyBitmaps()
    copyAudio()
    copyModels()

    
    shufflePalette()
    shuffleCreatures()
    shuffleWorld()
    shuffleAudio()
    shuffleModels()

    shutil.copy( "title.png", resourcePath + "Bitmaps/title.png" )

    
    # launch proteus
    if os.name == 'nt':
    	os.chdir( "../Proteus" )
    	subprocess.Popen(["Proteus.exe"])
    else:
    	subprocess.Popen(["../Proteus.app/Contents/MacOS/Proteus"])
    	
except EnvironmentError as e:
    print( "Error finding a file, have you copied your Proteus folder and pasted it into the 'ProDeus' folder?\n" )
    print( e )
    raw_input("Press Enter to continue...")